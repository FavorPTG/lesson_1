package com.example.app_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.io.Console;
import java.io.IOError;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private int paltoPrice = 70;
    private int paltoDiscount = 77;
    private int shlapaPrice = 25;
    private int shlapaDiscount = 37;
    private int costumePrice = 53;
    private int costumeDiscount = 44;
    private int sorochkaPrice = 19;
    private int sorochkaDiscount = 0;
    private int tufliPrice = 41;
    private int tufliDiscount = 32;

    int calculate(int money){
        int res = money - (
            (paltoPrice * (100-paltoDiscount) / 100) +
            (shlapaPrice * (100-shlapaDiscount) / 100) +
            (costumePrice * (100-costumeDiscount) / 100) +
            (sorochkaPrice * (100-sorochkaDiscount) / 100) +
            (tufliPrice * (100-tufliDiscount) / 100)
        );
        return res;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = findViewById(R.id.button);
        TextView resText = findViewById(R.id.resText);
        TextInputEditText input = findViewById(R.id.inputNumb);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable money = input.getText();

                try {
                    int res = Integer.parseInt(String.valueOf(calculate(Integer.parseInt(String.valueOf(money)))));

                    if(res > 0)
                        resText.setText("Денег хватит! Остаток: " + String.valueOf(res) + " монет");
                    else if(res == 0)
                        resText.setText("Денег хватит!");
                    else
                        resText.setText("Денег не хватит!");
                } catch (NumberFormatException e) {
                    resText.setText("Число не задано...");
                }
            }
        });
    }
}